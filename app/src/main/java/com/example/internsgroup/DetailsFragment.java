package com.example.internsgroup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


public class DetailsFragment extends Fragment {
    private TextView title, desc;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        osViewModel viewModel = ViewModelProviders.of(this.getActivity()).get(osViewModel.class);

        viewModel.getSelectedVersion().observe(this, item -> {
             displayDetails(viewModel.getVersionDetails(item));
        });
    }
    public void displayDetails(osmodel model){
        title.setText(model.getName());
        desc.setText(model.getDescription());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        title = (TextView) view.findViewById(R.id.osTitle);
        desc = (TextView)view.findViewById(R.id.osDesc);
        return view;
    }
}
