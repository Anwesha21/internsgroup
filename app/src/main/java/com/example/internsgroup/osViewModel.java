package com.example.internsgroup;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class osViewModel extends ViewModel {
    private final MutableLiveData<String> selectedVersion = new MutableLiveData<String>();
    private OSRepository repository = new OSRepository();

    public String[] getVersionList(String os){
        switch(os)
        {
            case "Android":
                 return repository.getAndroid();
            case "iOS":
                return repository.getiOS();
            case "Microsoft":
                return repository.getMicrosoft();

        }
        return null;
    }

    public void selectVersion(String VersionName) {
        selectedVersion.setValue(VersionName);
    }
    public MutableLiveData<String> getSelectedVersion() {
        return selectedVersion;
    }

    public osmodel getVersionDetails(String name){
        return repository.getosdetails(name);
    }
}
