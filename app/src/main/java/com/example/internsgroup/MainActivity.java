package com.example.internsgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void sendMessage(View view){
        Intent myintent = new Intent(this,sid_activity.class);
        startActivity(myintent);
    }
    public void vcActivityLaunch(View view){
        Intent vcIntent = new Intent(this, vcActivity.class);
        startActivity(vcIntent);
    }
    public void dvActivityLaunch(View view) {
        Intent dvIntent = new Intent(this, dvMainActivity.class);
        startActivity(dvIntent);
    }
    public void Anwesha ( View view) {
        Intent AM_intent = new Intent(this, AM_activity.class);
        startActivity(AM_intent);
    }
    public void team ( View view) {
        Intent Tintent = new Intent(this, Team_activity.class);
        startActivity(Tintent);
    }

    public void Pralipta ( View view) {
        Intent PR_intent = new Intent(this, PR_Activity.class);
        startActivity(PR_intent);
    }

}
