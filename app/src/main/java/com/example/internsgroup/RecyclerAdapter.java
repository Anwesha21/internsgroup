package com.example.internsgroup;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyOwnHolder> {
    private Context context;
    private int layout;
    private ArrayList<Model> recordList;

    public static class MyOwnHolder extends RecyclerView.ViewHolder {
        ImageView imageview,editview;
        TextView txtid,txtname, txtage, txtdob, txtphn, txtbranch, txtaddress;
        View row;
        public MyOwnHolder(@NonNull View itemView) {
            super(itemView);
            txtid = itemView.findViewById(R.id.listentry_id);
            imageview = itemView.findViewById(R.id.listentry_image);
            txtname = itemView.findViewById(R.id.listentry_name);
            txtage = itemView.findViewById(R.id.listentry_age);
            txtdob = itemView.findViewById(R.id.listentry_dob);
            txtphn = itemView.findViewById(R.id.listentry_phn);
            txtbranch = itemView.findViewById(R.id.listentry_branch);
            txtaddress = itemView.findViewById(R.id.listentry_address);
            editview = itemView.findViewById(R.id.edit_view);
            row = itemView;
        }
    }

    public RecyclerAdapter(Context context, int layout, ArrayList<Model> recordList){
        this.context = context;
        this.layout = layout;
        this.recordList = recordList;
    }
    @NonNull
    @Override
    public RecyclerAdapter.MyOwnHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(layout, parent, false);
        return new MyOwnHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull MyOwnHolder holder, int position) {
        Model model = recordList.get(position);
        holder.txtid.setText((Long.toString(model.getId())));
        holder.txtname.setText(model.getName());
        holder.txtage.setText(model.getAge());
        holder.txtdob.setText(model.getDob());
        holder.txtphn.setText(model.getPhn());
        holder.txtbranch.setText(model.getBranch());
        holder.txtaddress.setText(model.getAddress());

        View finalRow = holder.row;
        MyOwnHolder finalHolder = holder;
        holder.editview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent modifyIntent = new Intent(view.getContext(), modify_record.class);
                TextView id = view.findViewById(R.id.listentry_id);
                TextView name = view.findViewById(R.id.listentry_name);
                TextView age = view.findViewById(R.id.listentry_age);
                TextView phn = view.findViewById(R.id.listentry_phn);
                TextView dob = view.findViewById(R.id.listentry_dob);
                TextView branch = view.findViewById(R.id.listentry_branch);
                TextView address = view.findViewById(R.id.listentry_address);
                modifyIntent.putExtra("id", finalHolder.txtid.getText().toString());
                modifyIntent.putExtra("name",finalHolder.txtname.getText().toString());
                modifyIntent.putExtra("age",finalHolder.txtage.getText().toString());
                modifyIntent.putExtra("phn",finalHolder.txtphn.getText().toString());
                modifyIntent.putExtra("dob",finalHolder.txtdob.getText().toString());
                modifyIntent.putExtra("branch",finalHolder.txtbranch.getText().toString());
                modifyIntent.putExtra("address",finalHolder.txtaddress.getText().toString());
                finalRow.getContext().startActivity(modifyIntent);
            }
        });

        String recordImage = model.getImage();
        Glide.with(holder.row).load(recordImage).into(holder.imageview);
    }

    @Override
    public int getItemCount() {
        return recordList.size();
    }

    public void update(ArrayList<Model> results) {
        recordList = new ArrayList<>();
        recordList.addAll(results);
        notifyDataSetChanged();
    }


}
