package com.example.internsgroup;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import java.util.ArrayList;

public class RecordListAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private ArrayList<Model> recordList;


    public RecordListAdapter(Context context, int layout, ArrayList<Model> recordList) {
        this.context = context;
        this.layout = layout;
        this.recordList = recordList;
    }


    public void update(ArrayList<Model> results) {
        recordList = new ArrayList<>();
        recordList.addAll(results);
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return recordList.size();
    }

    @Override
    public Object getItem(int i) {
        return recordList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class ViewHolder {
        ImageView imageview,editview;
        TextView txtid,txtname, txtage, txtdob, txtphn, txtbranch, txtaddress;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View row = view;
        ViewHolder holder = new ViewHolder();

        if(row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);
            holder.txtid = row.findViewById(R.id.listentry_id);
            holder.imageview = row.findViewById(R.id.listentry_image);
            holder.txtname = row.findViewById(R.id.listentry_name);
            holder.txtage = row.findViewById(R.id.listentry_age);
            holder.txtdob = row.findViewById(R.id.listentry_dob);
            holder.txtphn = row.findViewById(R.id.listentry_phn);
            holder.txtbranch = row.findViewById(R.id.listentry_branch);
            holder.txtaddress = row.findViewById(R.id.listentry_address);
            holder.editview = row.findViewById(R.id.edit_view);
            row.setTag(holder);
        }
        else{
            holder = (ViewHolder)row.getTag();

        }


        Model model = recordList.get(i);
        holder.txtid.setText((Long.toString(model.getId())));
        holder.txtname.setText(model.getName());
        holder.txtage.setText(model.getAge());
        holder.txtdob.setText(model.getDob());
        holder.txtphn.setText(model.getPhn());
        holder.txtbranch.setText(model.getBranch());
        holder.txtaddress.setText(model.getAddress());

        View finalRow = row;
        ViewHolder finalHolder = holder;
        holder.editview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent modifyIntent = new Intent(view.getContext(), modify_record.class);
                TextView id = view.findViewById(R.id.listentry_id);
                TextView name = view.findViewById(R.id.listentry_name);
                TextView age = view.findViewById(R.id.listentry_age);
                TextView phn = view.findViewById(R.id.listentry_phn);
                TextView dob = view.findViewById(R.id.listentry_dob);
                TextView branch = view.findViewById(R.id.listentry_branch);
                TextView address = view.findViewById(R.id.listentry_address);
                modifyIntent.putExtra("id", finalHolder.txtid.getText().toString());
                modifyIntent.putExtra("name",finalHolder.txtname.getText().toString());
                modifyIntent.putExtra("age",finalHolder.txtage.getText().toString());
                modifyIntent.putExtra("phn",finalHolder.txtphn.getText().toString());
                modifyIntent.putExtra("dob",finalHolder.txtdob.getText().toString());
                modifyIntent.putExtra("branch",finalHolder.txtbranch.getText().toString());
                modifyIntent.putExtra("address",finalHolder.txtaddress.getText().toString());
                finalRow.getContext().startActivity(modifyIntent);
            }
        });

        String recordImage = model.getImage();
        Glide.with(row).load(recordImage).into(holder.imageview);
//        holder.imageview.setImageBitmap(
//                decodeSampledBitmapFromResource(recordImage, 100, 100));

//        File file = new File(recordImage);
//        Bitmap b;
//        try {
//            InputStream streamIn = new FileInputStream(file);
//            b = BitmapFactory.decodeStream(streamIn); //This gets the image
////                imageview.setImageBitmap(b);
//            holder.imageview.setImageBitmap(b);
//
//
//        } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }
//        Bitmap bitmap = BitmapFactory.decodeByteArray(recordImage, 0,recordImage.length);
        //Bitmap b = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(recordImage));

        return row;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String path,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }
}
