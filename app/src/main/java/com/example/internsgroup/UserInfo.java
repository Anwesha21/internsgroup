package com.example.internsgroup;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class UserInfo implements Parcelable {
    private String image;
    private String name;
    private String age;
    private String phone;
    private String dob;
    private String branch;
    private String address;

    public UserInfo(String image, String name, String age, String phone, String dob, String branch, String address) {
        this.image = image;
        this.name = name;
        this.age = age;
        this.phone = phone;
        this.dob = dob;
        this.branch = branch;
        this.address = address;
    }

    public UserInfo(Parcel parcel){
       String image = parcel.readString();
       String name = parcel.readString();
       String age = parcel.readString();
       String phone = parcel.readString();
       String dob = parcel.readString();
       String branch = parcel.readString();
       String address = parcel.readString();
    }

    public static final Creator<UserInfo> CREATOR = new Creator<UserInfo>() {
        @Override
        public UserInfo createFromParcel(Parcel in) {
            return new UserInfo(in);
        }

        @Override
        public UserInfo[] newArray(int size) {
            return new UserInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(image);
        parcel.writeString(name);
        parcel.writeString(age);
        parcel.writeString(phone);
        parcel.writeString(dob);
        parcel.writeString(branch);
        parcel.writeString(address);

    }

    public String getImage(){
        return image;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
