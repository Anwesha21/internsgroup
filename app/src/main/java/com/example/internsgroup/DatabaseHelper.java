package com.example.internsgroup;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String SQL_CREATE_ENTRIES =

            "CREATE TABLE " + FeedReaderContract.FeedEntry.TABLE_NAME + " (" +
                    FeedReaderContract.FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    FeedReaderContract.FeedEntry.COLUMN_NAME_image + " TEXT," +
                    FeedReaderContract.FeedEntry.COLUMN_NAME_name + " TEXT," +
                    FeedReaderContract.FeedEntry.COLUMN_NAME_age + " TEXT," +
                    FeedReaderContract.FeedEntry.COLUMN_NAME_phoneno + " INTEGER," +
                    FeedReaderContract.FeedEntry.COLUMN_NAME_DOB + " TEXT," +
                    FeedReaderContract.FeedEntry.COLUMN_NAME_branch + " TEXT," +
                    FeedReaderContract.FeedEntry.COLUMN_NAME_address + " VARCHAR," +
                    FeedReaderContract.FeedEntry.COLUMN_NAME_username + " VARCHAR," +
                    FeedReaderContract.FeedEntry.COLUMN_NAME_password + " VARCHAR)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + FeedReaderContract.FeedEntry.TABLE_NAME;

    public static final int DATABASE_VERSION = 8;
    public static final String DATABASE_NAME = "Project1.db";

    Context context;
    SQLiteDatabase database;
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public Cursor fetch() {
        database = getReadableDatabase();
        String[] columns = new String[] { FeedReaderContract.FeedEntry._ID, FeedReaderContract.FeedEntry.COLUMN_NAME_image, FeedReaderContract.FeedEntry.COLUMN_NAME_name, FeedReaderContract.FeedEntry.COLUMN_NAME_age,
                FeedReaderContract.FeedEntry.COLUMN_NAME_phoneno, FeedReaderContract.FeedEntry.COLUMN_NAME_DOB, FeedReaderContract.FeedEntry.COLUMN_NAME_branch, FeedReaderContract.FeedEntry.COLUMN_NAME_address};
        Cursor cursor = database.query(FeedReaderContract.FeedEntry.TABLE_NAME, columns, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        else{
        }
        return cursor;
    }

    public void insert(String image, String name, String age, String phoneno, String DOB, String branch, String address, String username, String password) {
        database = getWritableDatabase();
        ContentValues contentValue = new ContentValues();
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_image, image);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_name, name);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_age, age);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_phoneno, phoneno);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_DOB, DOB);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_branch, branch);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_address, address);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_username, username);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_password, password);
        long newRowId = database.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, contentValue);
    }

    public int update(long _id, String image,  String name, String age, String phoneno, String DOB, String branch, String address, String username, String password) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(FeedReaderContract.FeedEntry._ID, _id);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_image, image);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_name, name);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_age, age);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_phoneno, phoneno);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_DOB, DOB);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_branch, branch);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_address, address);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_username, username);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_password, password);

        int i = database.update(FeedReaderContract.FeedEntry.TABLE_NAME, contentValue, FeedReaderContract.FeedEntry._ID + " = " + _id, null);
        return i;
    }

    public void delete(long _id) {

        database.delete(FeedReaderContract.FeedEntry.TABLE_NAME, FeedReaderContract.FeedEntry._ID + "=" + _id, null);
    }

    public Cursor query(String query) {
        database = getReadableDatabase();
        Cursor cursor = database.rawQuery(query,null);
        return cursor;

    }

}
