package com.example.internsgroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;

import org.apache.commons.io.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class p1_Signup_2 extends AppCompatActivity implements View.OnClickListener {

    private EditText nm, dob, phn, add, age;
    Button submit;
    private Spinner spinner;
    File file;
    ImageView imageToUpload;
    private static final int RESULT_LOAD_IMAGE = 1;
    public static Uri selectedImage;
    public static final String EXTRA_MESSAGE = "com.example.internsgroup.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p1__signup_2);
        spinner =  findViewById(R.id.p1sbranch);


        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.names));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(myAdapter);

        nm = (EditText) findViewById(R.id.p1sname);
        dob = (EditText) findViewById(R.id.p1sdob);
        phn = (EditText) findViewById(R.id.p1spno);
        add = (EditText) findViewById(R.id.p1saddress);
        age = findViewById(R.id.p1sage);
        submit = (Button) findViewById(R.id.p1snext_btn);
        imageToUpload = (ImageView) findViewById(R.id.p1simage);
        submit.setOnClickListener(this);
        imageToUpload.setOnClickListener(this);
        Intent intent = this.getIntent();
        if(intent != null) {
            try {
                String strdata = intent.getExtras().getString("uniqid");
                if(strdata.equals("from_credentials"))
                {
                    UserInfo user = intent.getParcelableExtra("user");
                    nm.setText(user.getName());
                    dob.setText(user.getDob());
                    phn.setText(user.getPhone());
                    add.setText(user.getAddress());
                    age.setText(user.getAge());
                    imageToUpload.setImageURI(Uri.parse(user.getImage()));
                    if (user.getBranch() != null) {
                        int spinnerPosition = myAdapter.getPosition(user.getBranch());
                        spinner.setSelection(spinnerPosition);
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.p1simage:
                try {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, RESULT_LOAD_IMAGE);
                    } else {
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                break;
            case R.id.p1snext_btn:
                if (nm.getText().toString().equals("")) {
                    nm.setError("Enter valid full name");
                } else if (dob.getText().toString().equals("")) {
                    dob.setError("Enter DOB");
                } else if (phn.getText().toString().equals("")) {
                    phn.setError("Enter Phone No.");
                } else if (add.getText().toString().equals("")) {
                    add.setError("Enter Address");
                } else {
                    p1credentials();
                }
                break;
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String [] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case RESULT_LOAD_IMAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
                } else {
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            selectedImage = data.getData();
            Glide.with(this).load(selectedImage).into(imageToUpload);
//            imageToUpload.setImageURI(selectedImage);

//            String path = getRealPathFromURI(this, selectedImage);
//            String name = getFileName(selectedImage);


//            try {
//                insertInPrivateStorage(path, name);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

        }
        //imageSaveName1 = file.getAbsolutePath();
    }

    private void insertInPrivateStorage(String path, String name) throws IOException {
        FileOutputStream fos = openFileOutput(name, MODE_APPEND);

        file = new File(path);

        byte[] bytes = getBytesFromFile(file);

        fos.write(bytes);
        fos.close();

    }

    private byte[] getBytesFromFile(File file) throws IOException {
        byte[] data = FileUtils.readFileToByteArray(file);
        return data;
    }

    private String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private String getRealPathFromURI(Context context, Uri uri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null,
                null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return null;
    }

    public void p1credentials() {
        String spinnerstr = spinner.getSelectedItem().toString();
        UserInfo user = new UserInfo(
                selectedImage.toString(),
                nm.getText().toString(),
                age.getText().toString(),
                phn.getText().toString(),
                dob.getText().toString(),
                spinnerstr,
                add.getText().toString()
                );
        String message1 = "\tYOUR DETAILS::\n\n Name : " + nm.getText().toString() + "\n Age : " + age.getText().toString() + "\n Ph no. : " + phn.getText().toString();
        String message2 = "\n D.O.B : " + dob.getText().toString() +"\n Branch : " + spinnerstr + "\n Address : " + add.getText().toString();
        String message = message1 + message2;
        Intent cred_intent = new Intent(this, p1credentials.class);
        cred_intent.putExtra(EXTRA_MESSAGE, message);
        cred_intent.putExtra("user", user);
        startActivity(cred_intent);
    }

}