package com.example.internsgroup;

public class Model {
    private long id;
    private String image;
    private String name;
    private String age;
    private String dob;
    private String phn;
    private String branch;
    private String address;

    public Model(long id, String image, String name, String age, String dob, String phn, String branch, String address) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.age = age;
        this.dob = dob;
        this.phn = phn;
        this.branch = branch;
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhn() {
        return phn;
    }

    public void setPhn(String phn) {
        this.phn = phn;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
