package com.example.internsgroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class p1credentials extends AppCompatActivity implements View.OnClickListener {
    DatabaseHelper dbhelper;
    private EditText username, password;
    private String name, age, address, dob, phn, branch;
    private Uri setUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p1credentials);
        TextView textview = findViewById(R.id.p1cdetails);
        ImageView image = findViewById(R.id.userpic);
        Intent intent = getIntent();
        UserInfo user = intent.getParcelableExtra("user");
        String message = intent.getStringExtra(p1signup.EXTRA_MESSAGE);
        name = user.getName();
        age = user.getAge();
        dob = user.getDob();
        phn = user.getPhone();
        address = user.getAddress();
        branch = user.getBranch();
        setUri = Uri.parse(user.getImage());
        Glide.with(this).load(setUri).into(image);
        textview.setText(message);
        dbhelper = new DatabaseHelper(this);

        username = findViewById(R.id.p1cusername);
        password = findViewById(R.id.p1cpassword);

        Button submitButton = findViewById(R.id.button13);
        submitButton.setOnClickListener(this);
    }

    public void onClick(View view){
        if (username.getText().toString().equals("")) {
            username.setError("Username cannot be blank");
        } else if (password.getText().toString().equals("")) {
            password.setError("password cannot be blank");

        } else {
            submit();
        }
    }

    public void submit ( ) {
        String imageSaveName1 = null;
        try {
            Bitmap bitmap;
            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), setUri);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
            byte[] bt = byteArrayOutputStream.toByteArray();

            File file = new File(Environment.getExternalStorageDirectory(),"TESTTING");
            if (!file.exists()){
                file.mkdirs();
            }

            imageSaveName1 = file.getAbsolutePath()+"/"+System.currentTimeMillis();

            try {
                FileOutputStream fileOutputStream = new FileOutputStream(imageSaveName1);
                fileOutputStream.write(bt);
                fileOutputStream.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        dbhelper.insert(imageSaveName1,name, age,phn,dob,branch ,address, username.getText().toString(), password.getText().toString());

        Toast.makeText(this, " SUCCESSFULLY ADDED ", Toast.LENGTH_SHORT).show();
        Intent p1s_intent = new Intent(this, project1.class);
        startActivity(p1s_intent);
    }

    public void edit ( View view) {
        Intent p1_intent = new Intent(this, p1_Signup_2.class);
        UserInfo user = new UserInfo(setUri.toString(), name, age, phn, dob, branch, address);
        p1_intent.putExtra("user", user);
        p1_intent.putExtra("uniqid","from_credentials");
        startActivity(p1_intent);
    }



}
