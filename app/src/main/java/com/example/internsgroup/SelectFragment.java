package com.example.internsgroup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

public class SelectFragment extends Fragment {
    private AutoCompleteTextView textView;
    private osViewModel viewModel;
    private Spinner spinner;
    private Button submit;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_select, container,false);
        submit = (Button) view.findViewById(R.id.osSubmit);
        textView = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView2);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(),
                android.R.layout.simple_list_item_1, OS);
        textView.setAdapter(adapter);
        textView.setThreshold(1);
        spinner = view.findViewById(R.id.versionlist);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, new String []{"Versions"});
        spinner.setAdapter(myAdapter);
        return view;
    }

    private static final String[] OS = new String[] {
            "Android", "iOS", "Microsoft"
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this.getActivity()).get(osViewModel.class);
        textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String [] list = null;
                switch(textView.getText().toString()) {
                    case "Android":
                        list = viewModel.getVersionList("Android");
                        break;
                    case "iOS":
                        list = viewModel.getVersionList("iOS");
                        break;
                    case "Microsoft":
                        list = viewModel.getVersionList("Microsoft");
                        break;
                }
                ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_list_item_1, list);
                myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(myAdapter);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String name = spinner.getSelectedItem().toString();
                        viewModel.selectVersion(name);
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new DetailsFragment()).commit();
                        getActivity().findViewById(R.id.navigation_dashboard).setSelected(true);
                    }
                });


                }
            });
        }
    }


