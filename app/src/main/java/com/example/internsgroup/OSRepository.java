package com.example.internsgroup;

import java.util.HashMap;

public class OSRepository {
    private String [] Android, iOS, Microsoft;
    private HashMap<String, osmodel> osDetails;

    public String[] getAndroid() {
        if (Android == null) {
            Android = new String[15];
            Android[0] = "Cupcake";
            Android[1] = "Donut";
            Android[2] = "Eclair";
            Android[3] = "Froyo";
            Android[4] = "GingerBread";
            Android[5] = "HoneyComb";
            Android[6] = "Ice cream sandwich";
            Android[7] = "Jelly Bean";
            Android[8] = "KitKat";
            Android[9] = "Lollipop";
            Android[10] = "MarshMallow";
            Android[11] = "Nougat";
            Android[12] = "Oreo";
            Android[13] = "Pie";
            Android[14] = "Q";

        }

        return Android;
    }

    public String[] getiOS() {
        if (iOS == null) {
            iOS = new String[12];
            iOS[0] = "iOS 1";
            iOS[1] = "iOS 2";
            iOS[2] = "iOS 3";
            iOS[3] = "iOS 4";
            iOS[4] = "iOS 5";
            iOS[5] = "iOS 6";
            iOS[6] = "iOS 7";
            iOS[7] = "iOS 8";
            iOS[8] = "iOS 9";
            iOS[9] = "iOS 10";
            iOS[10] = "iOS 11";
            iOS[11] = "iOS 12";

        }

        return iOS;
    }

    public String[] getMicrosoft() {
        if (Microsoft == null) {
            Microsoft = new String[6];
            Microsoft[0] = "windows phone 7";
            Microsoft[1] = "windows phone 7.5";
            Microsoft[2] = "windows phone 7.8";
            Microsoft[3] = "windows phone 8";
            Microsoft[4] = "windows phone 8.1";
            Microsoft[5] = "windows phone 10";
        }

        return Microsoft;
    }
    public osmodel getosdetails(String name){
        if(osDetails==null) {
            createosdetailsmap();
        }
        return osDetails.get(name);
        }

    private void createosdetailsmap() {
        osDetails = new HashMap<String, osmodel>();

        osmodel api1 = new osmodel();
        api1.setName("API3 : CUPCAKE");
        api1.setdescription("With early 2009's Android 1.5 Cupcake release, the tradition of Android version names was born. Cupcake introduced numerous refinements to the Android interface, including the first on-screen keyboard.Cupcake also brought about the framework for third-party app widgets.");
        osDetails.put("Cupcake", api1);

        osmodel api2 = new osmodel();
        api2.setName("API4 : DONUT");
        api2.setdescription("Android 1.6, Donut, rolled into the world in the fall of 2009.It has the ability for the OS to operate on a variety of different screen sizes and resolutions . It also added support for CDMA networks like Verizon, which would play a key role in Android's imminent explosion.");
        osDetails.put("Donut", api2);

        osmodel api3 = new osmodel();
        api3.setName("API5 : ECLAIR");
        api3.setdescription(" Android 2.0 Eclair, emerged just six weeks after Donut; its \"point-one\" update, also called Eclair, came out a couple months later. Eclair was the first Android release to enter mainstream consciousness. The release's most transformative element was the addition of voice-guided turn-by-turn navigation and real-time traffic info. Navigation aside, it brought live wallpapers, speech-to-text function and it injected  iOS-exclusive pinch-to-zoom capability into Android.");
        osDetails.put("Eclair", api3);

        osmodel api4 = new osmodel();
        api4.setName("API6 : FROYO");
        api4.setdescription(" Froyo was unveiled on May 20, 2010.Froyo did deliver some important front-facing features, though, including the addition of the now-standard dock at the bottom of the home screen as well as the first incarnation of Voice Actions.");
        osDetails.put("Froyo", api4);

        osmodel api5 = new osmodel();
        api5.setName("API7 : GINGERBREAD");
        api5.setdescription("Android \"Gingerbread\" is the seventh version of Android and released in December 2010.\n" +
                "Features:\n" +
                "Updated user interface design, providing increased ease-of-use and efficiency.\n" +
                "Support for extra-large screen sizes and resolutions (WXGA and higher).\n" +
                "Native support for SIP VoIP internet telephony.\n ");
        osDetails.put("Gingerbread", api5);

        osmodel api6 = new osmodel();
        api6.setName("API8 : HONEYCOMB");
        api6.setdescription(" Android \"Honeycomb\" is the codename for the eighth version of Android, designed for devices with larger screen sizes, particularly tablets. Honeycomb debuted with the Motorola Xoom in February 2011.\n" +
                "Features:\n" +
                "Redesigned keyboard to make entering text easier on large screen devices such as tablets.\n" +
                "A Recent Apps view for multitasking.\n" +
                "Customizable home screens (up to five).\n" +
                "The Email and Contacts apps use a two-pane UI.\n");
        osDetails.put("Honeycomb", api6);

        osmodel api7 = new osmodel();
        api7.setName("API9 : ICE CREAM SANDWICH");
        api7.setdescription(" Android \"Ice Cream Sandwich\" is the ninth version of the Android mobile operating system developed by Google and was Unveiled on October 19, 2011.\n" +
                "The user interface of Android 4.0 represents an evolution of the design introduced by Honeycomb, although the futuristic aesthetics of Honeycomb were scaled back in favor of a flatter and cleaner feel with neon blue accenting, hard edges, and drop shadows for depth. Ice Cream Sandwich also introduces a new default system font, Roboto.\n");
        osDetails.put("Ice cream sandwich", api7);

        osmodel api8 = new osmodel();
        api8.setName("API10 : JELLY BEAN");
        api8.setdescription("Spread across three impactful Android versions, 2012 and 2013's Jelly Bean releases took ICS's fresh foundation and made meaningful strides in fine-tuning and building upon it. The releases added plenty of poise and polish into the operating system and went a long way in making Android more inviting for the average user. ");
        osDetails.put("Jelly bean", api8);

        osmodel api9 = new osmodel();
        api9.setName("API11 : KITKAT");
        api9.setdescription("Late-2013's KitKat release marked the end of Android's dark era, as the blacks of Gingerbread and the blues of Honeycomb finally made their way out of the operating system.Android 4.4 also saw the first version of \"OK, Google\" support — but in KitKat, the hands-free activation prompt worked only when your screen was already on and you were either at your home screen or inside the Google app. ");
        osDetails.put("Kitkat", api9);

        osmodel api10 = new osmodel();
        api10.setName("API12 : LOLLIPOP");
        api10.setdescription("Google essentially reinvented Android — again — with its Android 5.0 Lollipop release in the fall of 2014. Lollipop launched the still-present-today Material Design standard, which brought a whole new look that extended across all of Android, its apps and even other Google products. ");
        osDetails.put("lollipop", api10);

        osmodel api11 = new osmodel();
        api11.setName("API13 : MARSHMALLOW");
        api11.setdescription("In the grand scheme of things, 2015's Marshmallow was a fairly minor Android release — one that seemed more like a 0.1-level update than anything deserving of a full number bump. But it started the trend of Google releasing one major Android version per year and that version always receiving its own whole number. ");
        osDetails.put("Marshmallow", api11);

        osmodel api12 = new osmodel();
        api12.setName("API14 : NOUGAT");
        api12.setdescription("Google's 2016 Android Nougat releases provided Android with a native split-screen mode, a new bundled-by-app system for organizing notifications, and a Data Saver feature. Nougat added some smaller but still significant features, too, like an Alt-Tab-like shortcut for snapping between apps. ");
        osDetails.put("Nougat", api12);

        osmodel api13 = new osmodel();
        api13.setName("API15 : OREO");
        api13.setdescription(" Android Oreo added a variety of niceties to the platform, including a native picture-in-picture mode, a notification snoozing option, and notification channels that offer fine control over how apps can alert you.");
        osDetails.put("Oreo", api13);

        osmodel api14 = new osmodel();
        api14.setName("API8 : PIE");
        api14.setdescription("The freshly baked scent of Android Pie, a.k.a. Android 9, wafted into the Android ecosystem in early August 2018. Pie's most transformative change was its new gesture navigation system, which traded Android's traditional Back, Home, and Overview keys for a large, multifunctional Home button and a series of gesture-based commands.\n" +
                "Pie included some noteworthy new productivity features, too, such as a universal suggested-reply system for messaging notifications, a more effective method of screenshot management, and more intelligent systems for power management and screen brightness control.\n ");
        osDetails.put("Pie", api14);

        osmodel api15 = new osmodel();
        api15.setName("API19 : Q ");
        api15.setdescription("Google released its first beta preview of 2019's Android Q in March and followed it up with updated previews in April and May. Q's most prominent change thus far is a totally reimagined interface for Android gestures (yes, again) — this time doing away with the traditional Android Back button altogether and relying on a much more iPhone-reminiscent approach to system navigation. ");
        osDetails.put("Q", api15);

        osmodel IOS1 = new osmodel();
        IOS1.setName("IO1 : IOS1");
        IOS1.setdescription("PRALIPTA");
        osDetails.put("iOS 1", IOS1);


        osmodel IOS2 = new osmodel();
        IOS2.setName("IO2 : IOS2");
        IOS2.setdescription("PRABHASH");
        osDetails.put("iOS 2", IOS2);


        osmodel IOS3 = new osmodel();
        IOS3.setName("IO3 : IOS3");
        IOS3.setdescription("CHECK IT IN");
        osDetails.put("iOS 3", IOS3);


        osmodel WIN1 = new osmodel();
        WIN1.setName("WIN1 : WINDOWS PHONE 7 – October 21,2010");
        WIN1.setdescription("Windows Phone 7 is the only version of Windows Phone that features a kernel based on the Windows Embedded Compact 7 version of Windows Embedded CE, which was also used in Windows Mobile and Pocket PC systems.\n");
        osDetails.put("windows phone 7", WIN1);


        osmodel WIN2 = new osmodel();
        WIN2.setName("WIN2 : WINDOWS PHONE 7.5 – September 27,2011");
        WIN2.setdescription("In 2011, Microsoft released Windows Phone 7.5 Mango. The update included a mobile version of Internet Explorer 9 that supports the same web standards and graphical capability as the desktop version, multi-tasking of third-party apps, Twitter integration for the People Hub, and Windows Live SkyDrive access. A minor update released in 2012 known as \"Tango\", along with other bug fixes, lowered the hardware requirements to allow for devices with 800 MHz CPUs and 256 MB of RAM to run Windows Phone.\n");
        osDetails.put("windows phone 7.5", WIN2);


        osmodel WIN3 = new osmodel();
        WIN3.setName("WIN3 : WINDOWS PHONE 7.8 – January 2013");
        WIN3.setdescription("Microsoft announced Windows Phone 7.8 alongside Windows Phone 8 and began pushing the update to devices in January 2013. It added some features from the Windows NT based Windows Phone 8, backported to Windows Phone 7. These features included the updated start screen that allowed users to resize live tiles, additional theme colors, and an update to the lock screen that would optionally display the daily Bing homepage picture automatically. Windows Phone 7.8 does not support USSD.\n");
        osDetails.put("windows phone 7.8", WIN3);

        osmodel WIN4 = new osmodel();
        WIN4.setName("WIN4 : WINDOWS PHONE 8 – October 29,2012");
        WIN4.setdescription("On October 29, 2012, Microsoft released Windows Phone 8, a new generation of the operating system. Windows Phone 8 replaced its previously Windows CE-based architecture with one based on the Windows NT kernel with many components shared with Windows 8, allowing applications to be ported between the two platforms.\n");
        osDetails.put("windows phone 8", WIN4);

        osmodel WIN5 = new osmodel();
        WIN5.setName("WIN5 : WINDOWS PHONE 8.1 – April 2,2014");
        WIN5.setdescription("New features added include a notification center, support for the Internet Explorer 11 web browser, with tab syncing among Windows 8.1 devices, separate volume controls, and the option to skin and add a third column of live tiles to the Start Screen. Starting with this release, Microsoft dropped the requirement that all Windows Phone OEMs include a camera button and physical buttons for back, Start, and Search.\n");
        osDetails.put("windows phone 8.1", WIN5);

        osmodel WIN6 = new osmodel();
        WIN6.setName("WIN6 : WINDOWS PHONE 10 - January 21,2015");
        WIN6.setdescription("Windows 10 Mobile emphasized software using the Universal Windows Platform (UWP), which allowed apps to be designed for use across multiple Windows 10-based product families with nearly identical code, functionality, and adaptations for available input methods. When connected to an external display, devices could also render a stripped-down desktop interface similar to Windows on PCs, with support for keyboard and mouse input. Windows 10 Mobile featured Skype message integration, updated Office Mobile apps, notification syncing with other Windows 10 devices, support for the Microsoft Edge web browser, and other user interface improvements. Microsoft developed a middleware known as Windows Bridge to allow iOS Objective-C and Android C++ or Java software to be ported to run on Windows 10 Mobile with limited changes to code.\n");
        osDetails.put("windows phone 10", WIN6);

    }
}


