package com.example.internsgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class sid_activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sid_activity);
    }
    public void srform(View view){
        Intent intent1 = new Intent(this,SRFORM.class);
        startActivity(intent1);
    }
    public void calc(View view){
        Intent intent2 = new Intent(this,calculator.class);
        startActivity(intent2);
    }
    public void olx(View view){
        Intent intent3 = new Intent(this,OLXREG.class);
        startActivity(intent3);
    }
}
