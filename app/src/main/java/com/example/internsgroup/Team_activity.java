package com.example.internsgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Team_activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_activity);
    }
    public void project1 ( View view) {
        Intent p1intent = new Intent(this, project1.class);
        startActivity(p1intent);
    }
    public void project2 ( View view) {
        Intent p2intent = new Intent(this, project2.class);
        startActivity(p2intent);
    }
}
