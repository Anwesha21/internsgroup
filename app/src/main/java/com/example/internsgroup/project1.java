package com.example.internsgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class project1 extends AppCompatActivity {


    DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project1);
    }
    public void p1login ( View view) {
        Intent login_intent = new Intent(this, p1login.class);
        startActivity(login_intent);

    }
    public void p1signup ( View view) {
        Intent sign_intent = new Intent(this, p1_Signup_2.class);
        startActivity(sign_intent);
    }

    public int Login(String username,String password)
    {
        try
        {
            dbHelper = new DatabaseHelper(this);
            int i = 0;
            Cursor c = null;
            c = dbHelper.query("select * from "+FeedReaderContract.FeedEntry.TABLE_NAME+" where username =" + "\""+ username.trim() + "\""+" and password="+ "\""+ password.trim() + "\"");
            c.moveToFirst();
            i = c.getCount();
            c.close();
            return i;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return 0;
    }

    public void loginButton(View view){
        EditText usernameET = (EditText)findViewById(R.id.p1usernameET);
        EditText passwordET = (EditText)findViewById(R.id.p1passwordET);
        Button loginButton = (Button)findViewById(R.id.p1LoginButton);
        String username = usernameET.getText().toString();
        String password = passwordET.getText().toString();
        if(Login(username, password) == 1)
        {
            Log.i(null, "Validation Successful", null);
            Toast.makeText(this, "Validation Successful", Toast.LENGTH_SHORT).show();

            Intent login_intent = new Intent(this, p1login.class);
            startActivity(login_intent);
        }
        else {
            Toast.makeText(this, "Username or Password not found", Toast.LENGTH_SHORT).show();
        }
    }
}

