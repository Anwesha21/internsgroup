package com.example.internsgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class dvMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dv_main);
    }
    public void dvFbAlikeLaunch(View view){
        Intent dvIntent = new Intent(this, dvFbAlike.class);
        startActivity(dvIntent);
    }
}
