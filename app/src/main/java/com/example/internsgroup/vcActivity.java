package com.example.internsgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class vcActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vc);
    }

    public void vcActivity2Launch(View view) {
        Intent vc2Intent = new Intent(this, vcactivity2.class);
        startActivity(vc2Intent);
    }

    public void vcActivity3Launch(View view) {
        Intent vc3Intent = new Intent(this, vcactivity3.class);
        startActivity(vc3Intent);
    }
}
