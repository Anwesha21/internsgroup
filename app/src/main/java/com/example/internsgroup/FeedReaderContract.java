package com.example.internsgroup;

import android.provider.BaseColumns;

public final class FeedReaderContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private FeedReaderContract() {}

    /* Inner class that defines the table contents */
    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "UserDetails";
        public static final String COLUMN_NAME_image = "IMAGE";
        public static final String COLUMN_NAME_name = "NAME";
        public static final String COLUMN_NAME_age = "AGE";
        public static final String COLUMN_NAME_DOB = "DOB";
        public static final String COLUMN_NAME_phoneno = "PHONE_NUMBER";
        public static final String COLUMN_NAME_branch = "BRANCH";
        public static final String COLUMN_NAME_address = "ADDRESS";
        public static final String COLUMN_NAME_username = "USERNAME";
        public static final String COLUMN_NAME_password = "PASSWORD";


    }

}
