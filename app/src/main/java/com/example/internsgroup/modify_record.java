package com.example.internsgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class modify_record extends AppCompatActivity implements View.OnClickListener {
    private DatabaseHelper dbhelper;
    private EditText name, age, dob, contact, branch, address;
    private String id,
            str_image,
            str_name = null,
            str_age = null,
            str_dob = null,
            str_phn = null,
            str_branch = null,
            str_address = null,
            str_username = null,
            str_password = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_record);
        Intent intent = this.getIntent();
        id = intent.getStringExtra("id");
        name=findViewById(R.id.modify_name_editText);
        age=findViewById(R.id.modify_age_editText);
        contact=findViewById(R.id.modify_contact_editText);
        dob=findViewById(R.id.modify_dob_editText);
        branch=findViewById(R.id.modify_branch_editText);
        address=findViewById(R.id.modify_address_editText);
        try{
            dbhelper = new DatabaseHelper(this);
            Cursor cursor = dbhelper.query("select * from " + FeedReaderContract.FeedEntry.TABLE_NAME + " where _ID =" + "\"" + intent.getStringExtra("id") + "\"");
            cursor.moveToFirst();
            str_image = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_image));
            str_name = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_name));
            str_age = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_age));
            str_dob = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_DOB));
            str_phn = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_phoneno));
            str_branch = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_branch));
            str_address = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_address));
            str_username = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_username));
            str_password = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_password));

            cursor.close();

        }catch(Exception e){
            e.printStackTrace();
        }


        name.setText(str_name);
        age.setText(str_age);
        contact.setText(str_phn);
        dob.setText(str_dob);
        branch.setText(str_branch);
        address.setText(str_address);
        Button edit=findViewById(R.id.modify_edit);
        Button delete=findViewById(R.id.modify_delete);
        edit.setOnClickListener(this);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbhelper.delete(Long.parseLong(intent.getStringExtra("id")));
                Toast.makeText(modify_record.this, "Record Deleted", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(view.getContext(), p1login.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {
        dbhelper.update(Long.parseLong(id),  str_image,
                name.getText().toString(),age.getText().toString(),contact.getText().toString(),dob.getText().toString(),branch.getText().toString(),address.getText().toString(),
                str_username, str_password);
        Toast.makeText(modify_record.this, "Record Modified", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(view.getContext(), p1login.class);
        startActivity(intent);
    }
}
