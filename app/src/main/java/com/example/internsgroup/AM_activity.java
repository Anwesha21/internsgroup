package com.example.internsgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AM_activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_am_activity);
    }

    public void AM_whatsapp(View view) {
        Intent AM_intent = new Intent(this, AM_whatsappActivity.class);
        startActivity(AM_intent);
    }
}
