package com.example.internsgroup;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class DatabaseManager {
    private DatabaseHelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DatabaseManager(Context c) {
        context = c;
    }

    public DatabaseManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

//    public void insert(String name, String age, String phoneno, String DOB, String branch, String address) {
//        ContentValues contentValue = new ContentValues();
//        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_name, name);
//        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_age, age);
//        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_phoneno, phoneno);
//        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_DOB, DOB);
//        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_branch, branch);
//        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_address, address);
//
//        database.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, contentValue);
//    }

    public Cursor fetch() {
        String[] columns = new String[] { FeedReaderContract.FeedEntry._ID, FeedReaderContract.FeedEntry.COLUMN_NAME_name, FeedReaderContract.FeedEntry.COLUMN_NAME_age,
                FeedReaderContract.FeedEntry.COLUMN_NAME_phoneno, FeedReaderContract.FeedEntry.COLUMN_NAME_DOB, FeedReaderContract.FeedEntry.COLUMN_NAME_branch, FeedReaderContract.FeedEntry.COLUMN_NAME_address};
        Cursor cursor = database.query(FeedReaderContract.FeedEntry.TABLE_NAME, columns, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        else{
        }
        return cursor;
    }

    public int update(long _id, String name, String age, int phoneno, String DOB, String branch, String address) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_name, name);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_age, age);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_phoneno, phoneno);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_DOB, DOB);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_branch, branch);
        contentValue.put(FeedReaderContract.FeedEntry.COLUMN_NAME_address, address);
        int i = database.update(FeedReaderContract.FeedEntry.TABLE_NAME, contentValue, FeedReaderContract.FeedEntry._ID + " = " + _id, null);
        return i;
    }

    public void delete(long _id) {
        database.delete(FeedReaderContract.FeedEntry.TABLE_NAME, FeedReaderContract.FeedEntry._ID + "=" + _id, null);
    }
}
