package com.example.internsgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class SRFORM extends AppCompatActivity {

    public static final String EXTRA_MESSAGE="com.example.internsgroup.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_srform);
    }

    public void sendMessage(View view)
    {
        Intent intent = new Intent(this, DisplayMessage.class);
        EditText editText = findViewById(R.id.srt1);
        EditText editText1 = findViewById(R.id.srt2);
        EditText editText2 = findViewById(R.id.srt3);
        EditText editText3 = findViewById(R.id.srt4);
        EditText editText4 = findViewById(R.id.srt5);
        RadioGroup rg = findViewById(R.id.srrg);
        int id = rg.getCheckedRadioButtonId();
        RadioButton rb = findViewById(id);
        String message1 = "\t !! WELCOME !! \n\n NAME : " +editText.getText().toString() + "\n AGE : " + editText1.getText().toString() + "\n PHONE NO. : "+editText2.getText().toString();
        String message2 = "\n EMAIL ID : "+editText3.getText().toString()+"\n ADDRESS : "+editText4.getText().toString()+"\n GENDER : "+rb.getText().toString();
        String message = message1 + message2;
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
}
