package com.example.internsgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

public class p1signup extends AppCompatActivity implements View.OnClickListener {
    private static final int RESULT_LOAD_IMAGE =1;

    public static final String EXTRA_MESSAGE="com.example.internsgroup.MESSAGE";
    public static String spinner;
    ImageView imageToUpload;
    Button bUploadImage;
    public static EditText editText;
    public static EditText editText1;
    public static EditText editText2;
    public static EditText editText3;
    public static EditText editText4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p1signup);

        Spinner myspinner = (Spinner) findViewById(R.id.p1sbranch);

        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(p1signup.this,
                android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.names));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        myspinner.setAdapter(myAdapter);


        imageToUpload = (ImageView) findViewById(R.id.p1simage);
        bUploadImage = (Button) findViewById(R.id.bUploadImage);

        imageToUpload.setOnClickListener(this);
        bUploadImage.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.p1simage:
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent,RESULT_LOAD_IMAGE);

                break;
            case R.id.bUploadImage:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null){
            Uri selectedImage = data.getData();
            imageToUpload.setImageURI(selectedImage);


        }
    }

    public void NEXT ( View view) {
        Intent p1_intent = new Intent(this, p1credentials.class);
        editText = findViewById(R.id.p1sname);
        editText1 = findViewById(R.id.p1sage);
        editText2 = findViewById(R.id.p1sdob);
        editText3 = findViewById(R.id.p1spno);
        editText4 = findViewById(R.id.p1saddress);
        Spinner spinner1 = (Spinner)findViewById(R.id.p1sbranch);
        spinner = spinner1.getSelectedItem().toString();
        String message1 = "\tYOUR DETAILS::\n\n NAME : " +editText.getText().toString() + "\n AGE : " + editText1.getText().toString() + "\n DOB. : "+editText2.getText().toString();
        String message2 = "\n PH NO. : "+editText3.getText().toString()+"\n ADDRESS : "+editText4.getText().toString() + "\n BRANCH : "+spinner;
        String message = message1 + message2;
        p1_intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(p1_intent);
    }


}
