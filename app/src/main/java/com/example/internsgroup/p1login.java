package com.example.internsgroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

public class p1login extends AppCompatActivity {

    private DatabaseHelper dbhelper;
    private ListView listView;
    private RecyclerView recyclerview;
    private RecyclerAdapter recyclerAdapter;
    private RecordListAdapter myAdapter;
    private ArrayList<Model> mlist;
    private RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_p1login);

        SimpleCursorAdapter adapter;
        mlist = new ArrayList<>();
//        myAdapter = new RecordListAdapter(this, R.layout.p1_listentry, mlist);
        recyclerAdapter =  new RecyclerAdapter(this, R.layout.p1_listentry,mlist);

//
//        final String[] from = new String[]{FeedReaderContract.FeedEntry.COLUMN_NAME_image, FeedReaderContract.FeedEntry.COLUMN_NAME_name, FeedReaderContract.FeedEntry.COLUMN_NAME_age,
//                FeedReaderContract.FeedEntry.COLUMN_NAME_phoneno, FeedReaderContract.FeedEntry.COLUMN_NAME_DOB, FeedReaderContract.FeedEntry.COLUMN_NAME_branch, FeedReaderContract.FeedEntry.COLUMN_NAME_address};
//
//        final int[] to = new int[]{R.id.listentry_image, R.id.listentry_name, R.id.listentry_age, R.id.listentry_phn, R.id.listentry_dob, R.id.listentry_branch, R.id.listentry_address};


        dbhelper = new DatabaseHelper(this);
        Cursor cursor;
        try {

            recyclerview = findViewById(R.id.p1login_listview);
            layoutManager = new LinearLayoutManager(this);
            recyclerview.setLayoutManager(layoutManager);



//            recyclerview.setE(findViewById(R.id.p1login_empty_textview));
            recyclerview.setAdapter(recyclerAdapter);

            cursor = dbhelper.fetch();
            int count = cursor.getCount();

            mlist.clear();
            cursor.moveToFirst();
            do {
                long id = cursor.getLong(cursor.getColumnIndex(FeedReaderContract.FeedEntry._ID));
                String image = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_image));
                String name = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_name));
                String age = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_age));
                String phone = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_phoneno));
                String dob = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_DOB));
                String branch = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_branch));
                String address = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_address));

                mlist.add(new Model(id, image,  name, age, phone, dob, branch, address));

            }while (cursor.moveToNext());
            recyclerAdapter.notifyDataSetChanged();
            if (mlist.isEmpty()) {
                recyclerview.setVisibility(View.GONE);
                findViewById(R.id.p1login_empty_textview).setVisibility(View.VISIBLE);
            }
            else {
                recyclerview.setVisibility(View.VISIBLE);
                findViewById(R.id.p1login_empty_textview).setVisibility(View.GONE);
            }
            //while(cursor.moveToNext()) {



//            adapter = new SimpleCursorAdapter(this, R.layout.p1_listentry, cursor, from, to, 0);
//            adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
//                public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
//                    if (view.getId() == R.id.listentry_image) {
//
//                        Bitmap b = null;
//                        String path = (cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_image)));
//                        File file = new File(path);
//                        try {
//                            InputStream streamIn = new FileInputStream(file);
//                            b = BitmapFactory.decodeStream(streamIn); //This gets the image
//                            ImageView imageview = (ImageView) view;
////                            imageview.setImageBitmap(b);
//
//                        } catch (FileNotFoundException e) {
//                            e.printStackTrace();
//                        }
//
//
//
//
//                        return true;
//                    } else {  // Process the rest of the adapter with default settings.
//                        return false;
//                    }
//                }
//            });
//            myAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.search_item);
        SearchView searchView = (SearchView) menuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                ArrayList< Model> results = new ArrayList<>();

                for(Model m : mlist) {
                    if (m.getName().contains(newText)){
                        results.add(m);
                    }

//                    ((RecordListAdapter)listView.getAdapter()).update(results);
                    ((RecyclerAdapter)recyclerview.getAdapter()).update(results);
                }
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }
}