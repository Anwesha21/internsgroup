import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyHelper  extends SQLiteOpenHelper {

    private static final String dbname="PROJECT1db";
    private static final int version = 1;


    public MyHelper(Context context){
        super(context, dbname, null, version);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE DATA (_id INTEGER PRIMARY KEY AUTOINCREMENT,NAME TEXT ,AGE INTEGER, PHONENO INTEGER, DOB INTEGER, BRANCH STRING, ADDRESS STRING )";
        db.execSQL(sql);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
